package facci.gomezolives.mariafernanda.VeteraniaJirafita.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import facci.gomezolives.mariafernanda.VeteraniaJirafita.DBSQlite.DBSqliteHelper;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.R;

public class MainActivity extends AppCompatActivity {

    EditText txtUsuario, txtContrasenia;
    Button btnLogin, btnRegistro;
    String usuario, contrasenia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = findViewById(R.id.txtUsuario);
        txtContrasenia = findViewById(R.id.txtContrasenia);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegistro = findViewById(R.id.btnRegistro);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario = txtUsuario.getText().toString();
                contrasenia = txtContrasenia.getText().toString();
                IniciarSesion(usuario,contrasenia);
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });

    }

    private void IniciarSesion(String usuario, String contrasenia) {
        if (!usuario.isEmpty() && !contrasenia.isEmpty()){
            try {
                DBSqliteHelper conexion = new DBSqliteHelper(this, "OPTATIVA", null, 1);
                SQLiteDatabase db = conexion.getReadableDatabase();
                Cursor fila = db.rawQuery("select * from Usuario where usuario=? and contrasenia=?", new String[]{usuario,contrasenia});
                if (fila.moveToFirst()){
                    Toast.makeText(this, "LOGUEADO", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, MenuPrincipal.class);
                    intent.putExtra("nombre", fila.getString(3));
                    intent.putExtra("user", fila.getString(1));
                    startActivity(intent);
                }else {
                        Toast.makeText(this, "DATOS INCORRECTOS", Toast.LENGTH_SHORT).show();
                    }
                db.close();
            }catch (Exception e){
                Log.e("BDSQLITE", e.getMessage());
            }

        }else if (usuario.isEmpty()){
            txtUsuario.setError("CAMPO OBLIGATORIO");
            txtUsuario.requestFocus();
        }else if (contrasenia.isEmpty()){
            txtContrasenia.setError("CAMPO OBLIGATORIO");
            txtContrasenia.requestFocus();
        }

    }
}