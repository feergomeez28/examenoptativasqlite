package facci.gomezolives.mariafernanda.VeteraniaJirafita.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import facci.gomezolives.mariafernanda.VeteraniaJirafita.DBSQlite.DBSqliteHelper;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.R;

public class MostrarActivity extends AppCompatActivity {

    TextView tv_tipo, tv_concepto, tv_fecha, tv_valor;
    Button btnEliminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);

        tv_tipo = findViewById(R.id.tv_tipo);
        tv_concepto = findViewById(R.id.tv_concepto);
        tv_fecha = findViewById(R.id.tv_fecha);
        tv_valor = findViewById(R.id.tv_valor);
        btnEliminar = findViewById(R.id.btnEliminar);

        final int id = getIntent().getExtras().getInt("id");
        String tipo = getIntent().getExtras().getString("tipo");
        String concepto = getIntent().getExtras().getString("concepto");
        String fecha = getIntent().getExtras().getString("fecha");
        int valor = getIntent().getExtras().getInt("valor");

        tv_tipo.setText("TIPO: " + tipo);
        tv_concepto.setText("CONCEPTO: " + concepto);
        tv_fecha.setText("FECHA: " + fecha);
        tv_valor.setText("VALOR: $" + valor);

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DBSqliteHelper conexion = new DBSqliteHelper(getApplicationContext(), "OPTATIVA", null, 1);
                SQLiteDatabase db = conexion.getWritableDatabase();

                db.delete("Cita", "id="+id,null );
                db.close();
                Toast.makeText(getApplicationContext(), "Se ha eliminado la cita con éxito", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}