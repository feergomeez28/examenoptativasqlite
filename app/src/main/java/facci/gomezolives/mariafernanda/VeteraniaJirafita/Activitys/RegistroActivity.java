package facci.gomezolives.mariafernanda.VeteraniaJirafita.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.lang.reflect.Array;

import facci.gomezolives.mariafernanda.VeteraniaJirafita.DBSQlite.DBSqliteHelper;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.R;

public class RegistroActivity extends AppCompatActivity {

    EditText txtNick, txtNombre, txtPass;
    Button btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txtNick = findViewById(R.id.txtNick);
        txtNombre = findViewById(R.id.txtNombre);
        txtPass = findViewById(R.id.txtPassword);
        btnRegistrar = findViewById(R.id.btnRegistrar);


        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Registrar();
            }
        });

    }

    private void Registrar() {

        //Establecer conexion con la clase OpenHelper para acceder a la base de datos
        DBSqliteHelper conexion = new DBSqliteHelper(this, "OPTATIVA", null, 1);
        //Pone a la base de datos en modo de escritura
        SQLiteDatabase db = conexion.getWritableDatabase();

        //Asigna variables de tipo String a los campos de textos
        String nick = txtNick.getText().toString();
        String nombre = txtNombre.getText().toString();
        String pass = txtPass.getText().toString();

        //Validacion de campos
        if (!nick.isEmpty() && !nombre.isEmpty() && !pass.isEmpty()){

            try {

                Boolean chknick = conexion.chknick(nick);
                if (chknick==true){
                    //Clase ContentValues para guardar los valores
                    ContentValues valores = new ContentValues();
                    //Metodo put para agregar al objeto ContentValues
                    valores.put("usuario", nick);
                    valores.put("contrasenia", pass);
                    valores.put("nombre", nombre);
                    //Inserta a la tabla Jugador los datos que se guardan en el objeto ContentValues
                    db.insert("Usuario", null, valores);
                    Toast.makeText(this, "Se agregó el usuario", Toast.LENGTH_SHORT).show();
                    //Se cierra la base de datos
                    db.close();
                    txtNick.setText("");
                    txtNombre.setText("");
                    txtPass.setText("");
                    Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                    startActivity(intent);
                    this.finish();

                }else {
                    Toast.makeText(getApplicationContext(), "El usuario ya existe", Toast.LENGTH_SHORT).show();
                }

            }catch (Exception e){
                Log.e("DBSQLITE", e.getMessage());
            }
        }else if (nick.isEmpty()){
            txtNick.setError("Campo Obligatorio");
            txtNick.requestFocus();
        }else if (nombre.isEmpty()){
            txtNombre.setError("Campo Obligatorio");
            txtNombre.requestFocus();
        }else if (pass.isEmpty()){
            txtPass.setError("Campo Obligatorio");
            txtPass.requestFocus();
        }

    }
}