package facci.gomezolives.mariafernanda.VeteraniaJirafita;

import java.util.Date;

public class Cita {

    int id;
    String tipo;
    String concepto;
    String fecha;
    int valor;

    public Cita() {
    }

    public Cita(int id, String tipo, String concepto, String fecha, int valor) {
        this.id = id;
        this.tipo = tipo;
        this.concepto = concepto;
        this.fecha = fecha;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}
