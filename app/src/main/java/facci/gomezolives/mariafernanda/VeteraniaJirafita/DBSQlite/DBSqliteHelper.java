package facci.gomezolives.mariafernanda.VeteraniaJirafita.DBSQlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBSqliteHelper extends SQLiteOpenHelper {

    public DBSqliteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Usuario(id INTEGER PRIMARY KEY AUTOINCREMENT, usuario text, contrasenia text, nombre text)");
        db.execSQL("CREATE TABLE Cita(id INTEGER PRIMARY KEY AUTOINCREMENT, tipo text, concepto text, fecha date, valor int)");
        db.execSQL("INSERT INTO Usuario (usuario,contrasenia, nombre) VALUES ('FerGomez', '123456', 'Fernanda')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS Usuario");
        onCreate(db);
    }

    public Boolean chknick (String nick){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Usuario where usuario = ?", new String[]{nick} );
        if (cursor.getCount()>0) return false;
        else return true;
    }
}
