package facci.gomezolives.mariafernanda.VeteraniaJirafita.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import facci.gomezolives.mariafernanda.VeteraniaJirafita.Activitys.MostrarActivity;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.Cita;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.DBSQlite.DBSqliteHelper;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.R;

public class ConsultaFragment extends Fragment {

    Spinner stipo;
    String tipo;
    ListView lvtipo;
    ArrayList<String>  listaInfo;
    ArrayList<Cita> listaCitas;
    Button btnConsultarTipo;

    public ConsultaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_consulta, container, false);

        stipo = vista.findViewById(R.id.spinnerT);
        lvtipo = vista.findViewById(R.id.lvTipo);

        btnConsultarTipo = vista.findViewById(R.id.btnConsultarTipo);

        ArrayAdapter<CharSequence> items = ArrayAdapter.createFromResource(getActivity(),
                R.array.tipo, R.layout.support_simple_spinner_dropdown_item);
        stipo.setAdapter(items);

        stipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tipo = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnConsultarTipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consultarCitas(tipo);
            }
        });

        return vista;
    }

    private void consultarCitas(String tipo) {
        DBSqliteHelper conexion = new DBSqliteHelper(getActivity(), "OPTATIVA", null, 1);
        SQLiteDatabase db = conexion.getReadableDatabase();

        Cita cita =null;
        listaCitas = new ArrayList<Cita>();

        Cursor cursor = db.rawQuery("Select * from Cita where tipo=?", new String[]{tipo});

        if (cursor.getCount()==0){
            Toast.makeText(getActivity(), "No hay citas", Toast.LENGTH_SHORT).show();
            lvtipo.setAdapter(null);
        }else {

            while (cursor.moveToNext()){
                cita = new Cita();
                cita.setId(cursor.getInt(0));
                cita.setTipo(cursor.getString(1));
                cita.setConcepto(cursor.getString(2));
                cita.setFecha(cursor.getString(3));
                cita.setValor(cursor.getInt(4));

                listaCitas.add(cita);
            }

            mostrarLista();

        }

    }

    private void mostrarLista() {
        listaInfo = new ArrayList<String>();
        for (int i=0; i<listaCitas.size();i++){
            listaInfo.add(listaCitas.get(i).getTipo()
                    + " - $"
                    + listaCitas.get(i).getValor());

            ArrayAdapter adaptador = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, listaInfo);
            lvtipo.setAdapter(adaptador);

            lvtipo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                    Intent intent = new Intent(getActivity(), MostrarActivity.class);
                    intent.putExtra("id", listaCitas.get(position).getId());
                    intent.putExtra("tipo", listaCitas.get(position).getTipo());
                    intent.putExtra("concepto", listaCitas.get(position).getConcepto());
                    intent.putExtra("fecha", listaCitas.get(position).getFecha());
                    intent.putExtra("valor", listaCitas.get(position).getValor());
                    startActivity(intent);

                }
            });


        }

    }
}