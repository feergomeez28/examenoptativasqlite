package facci.gomezolives.mariafernanda.VeteraniaJirafita.Fragments;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import facci.gomezolives.mariafernanda.VeteraniaJirafita.Activitys.MainActivity;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.Activitys.RegistroActivity;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.DBSQlite.DBSqliteHelper;
import facci.gomezolives.mariafernanda.VeteraniaJirafita.R;

public class RegistrarCita extends Fragment {

    TextView lblFecha;
    RadioGroup rgTipo;
    RadioButton rb1;
    Spinner spinner;
    Button btnCalendario, btnAgendar;
    View vista;
    String concepto;

    public RegistrarCita() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista =  inflater.inflate(R.layout.fragment_registrar_cita, container, false);
        lblFecha = vista.findViewById(R.id.lblFecha);

        rgTipo = vista.findViewById(R.id.radioGroupTipo);

        btnAgendar = vista.findViewById(R.id.btnAgendarCita);
        btnCalendario = vista.findViewById(R.id.btnCalendario);
        spinner = vista.findViewById(R.id.spinnerConcepto);

        lblFecha.setText("2020-" + Calendar.MONTH + "-" + Calendar.DAY_OF_MONTH);

        ArrayAdapter<CharSequence> items = ArrayAdapter.createFromResource(getActivity(),
                R.array.concepto, R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(items);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                concepto = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnCalendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int anio = calendar.get(Calendar.YEAR);
                int mes = calendar.get(Calendar.MONTH);
                int dia = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog =
                        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayofMonth) {
                        String fecha = year + "-" + month + "-" + dayofMonth;
                        lblFecha.setText(fecha);
                    }
                }, 2020, mes, dia);
                datePickerDialog.show();
            }
        });


        btnAgendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fecha = lblFecha.getText().toString();

                int checked = rgTipo.getCheckedRadioButtonId();
                rb1 = vista.findViewById(checked);
                String tipo = rb1.getText().toString();

                RegistrarEnBD(tipo, concepto, fecha);




            }
        });

        return vista;
    }

    private void RegistrarEnBD(String tipo, String concepto, String fecha) {

            DBSqliteHelper conexion = new DBSqliteHelper(getActivity(), "OPTATIVA", null, 1);
            SQLiteDatabase db = conexion.getWritableDatabase();

            try {
                ContentValues valores = new ContentValues();
                valores.put("tipo", tipo);
                valores.put("concepto", concepto);
                valores.put("fecha", fecha);
                valores.put("valor", 10);
                db.insert("Cita", null, valores);
                db.close();
                Toast.makeText(getActivity(), "Se agendó la cita con éxito", Toast.LENGTH_SHORT).show();
            }catch (Exception e){
                Log.e("DBSQLITE", e.getMessage());
            }

    }

}